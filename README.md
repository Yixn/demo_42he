#README

Create a .env file in the root directory with the following content. 
```
API_KEY=SETME
API_URL=SETME
```
  
Change the SETME to the proper values and run
<tt>bundle && bundle exec puma -p3000 --environment=development</tt>.. 

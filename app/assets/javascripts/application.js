// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .
//= require bootstrap-sprockets
//= require chosen-jquery


function sortTable(table) {
  sort = this;
  table = $('table');
  rows = table.find('tbody > tr[data-lat]');
  rows.sort(function (a, b) {
      var keyA = parseInt($(a).find('.distance_td span').text());
      var keyB = parseInt($(b).find('.distance_td span').text());
      console.log(keyA)
      console.log(keyB)
      return (keyA > keyB) ? 1 : 0;
  });
  $.each(rows, function (index, row) {
      table.append(row);
  });
}
function prepare_address(e){
  address = "";
  if(e["data"]["address"]["road"] !== undefined){
    address += e["data"]["address"]["road"] + ", ";
  }
  // thank you?!
  if(e["data"]["address"]["address26"] !== undefined){
    address += e["data"]["address"]["address26"] + ", ";
  }
  if(e["data"]["address"]["postcode"] !== undefined){
    address += e["data"]["address"]["postcode"];
    if(e["data"]["address"]["city"] !== undefined){
      address += " " + e["data"]["address"]["city"] + ", ";
    }
  }else if(e["data"]["address"]["county"] !== undefined){
    address += e["data"]["address"]["county"] + ", ";
  }
  if(e["data"]["address"]["country_code"] !== undefined){
    address += e["data"]["address"]["country_code"].toUpperCase();
  }
  return address;
}
function prepare_coordinate_data(){
  coordinate_json = {"people": {}};
  $("tr[data-lat]").each(function(i, e){
    e = $(e)
    coordinate_json["people"][e.data("id")] = [e.data("lat"), e.data("lon")];
  })
  coordinate_json["coordinates"] = $(".chosen-select").chosen().val();
  coordinate_json["radius"] = $(".slider").val();
  return coordinate_json;
}

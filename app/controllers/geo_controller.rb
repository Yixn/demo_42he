class GeoController < ApplicationController
  def check_address
    locations = Geocoder.search(params[:q])
    render json: locations
  end

  def check_radius
    location = JSON.parse(params[:coordinates])
    radius = params[:radius].to_i
    people_in_radius = {}
    params[:people].each do |id, person_coords| 
      distance = Geocoder::Calculations.distance_between(location, person_coords)
      puts distance
      if( distance <= radius)
        people_in_radius[id] = distance.round
      end
    end
    render json: people_in_radius
  end
end

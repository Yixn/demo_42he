module PeopleHelper
  def full_name(person)
    "#{person['name']}, #{person['first_name']}"
  end

  def primary_address(person)
    if person["addrs"].empty? 
      primary_adress = {"full_address" => nil, "error" => "<span class='address_error'><i class='fa fa-times red' aria-hidden='true'></i>No Address</span>"}

    else
      if person["addrs"].count == 1
        primary_adress = person["addrs"][0]
      else
        primary_adress = person["addrs"].select {|address| address["primary"] == true }[0] || person["addrs"][0]
      end

      primary_adress["full_address"] = full_address(primary_adress)
      geo = Geocoder.search(primary_adress["full_address"])[0]
      if geo != nil
        primary_adress["lat"] = geo.data["lat"]
        primary_adress["lon"] = geo.data["lon"]
      else
        primary_adress = {"full_address" => nil, "error" => "<span class='address_error'><i class='fa fa-times red' aria-hidden='true'></i>Invalid Address</span>"}
      end
    end
    primary_adress
  end

  def person_geo_data_tag(address)
    if address["full_address"].present?
     " data-lat=#{address['lat']} data-lon=#{address['lon']} "
    end
  end

  def full_address(address_obj)
    return  "-" if address_obj.blank?
    full_address = ""
    (full_address += "#{address_obj['street']}, ") if address_obj['street'].present?
    (full_address += "#{address_obj['zip']} #{address_obj['city']}, ") if address_obj['zip'].present?
    (full_address += "#{address_obj['country_code']}") if address_obj['country_code'].present?
    full_address
  end
end

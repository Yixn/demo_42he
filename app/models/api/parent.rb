module Api
  class Parent
    def initialize
      @base_url = ENV['API_URL']
      @header = {'Content-Type' => 'application/json', 'X-apikey' => ENV['API_KEY']}
    end

    def get_data(url, includes = nil)
      include_param = (includes.present? ? "?includes=#{includes}" : "")
      response = HTTParty.get(@base_url + "/" +url+".json"+include_param , headers: @header)
      # try catch missing
      JSON.parse(response.body)
    end

  end
end

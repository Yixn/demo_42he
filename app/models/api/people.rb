module Api
  class People < Api::Parent
    def get
      get_data("people", "addrs")
    end
  end
end
